#!/bin/bash

set -e

[[ $# -lt 1 ]] && { echo "Missing html directory"; exit 1; }

cd $1

[[ -f index.html ]] && { echo "Moving index.html to index.bak"; mv index.html index.bak; }
html=$(find -name "*.html")

echo "Processing: $html"

## Computing image base url
gitlab_url="https://gitlab.com/nsvir/instantcine-newsletter/raw/master/"
image_path="$(pwd | cut -c $(git rev-parse --show-toplevel | wc -c)-)/images/"
image_url="$gitlab_url$image_path"
sed_image_url=$(echo "$image_url" | sed "s/\//\\\\\//g")

echo "Sedding: $sed_image_url"
cat "$html" | sed "s/images\//$sed_image_url\//g" > index.html

echo "Created index.html"
